from django.shortcuts import render, redirect

# Create your views here.

# The from keyword allows importing of necessary classes/modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

# Local imports
from .models import GroceryItem

# import the built in User Model
from django.contrib.auth.models import User

# to use the template created:
# from django.template import loader

from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	# template = loader.get_template("index.html")
	context = {
		'groceryitem_list': groceryitem_list,
		"user": request.user
	}
	# output = ", ".join([groceryitem.task_name for groceryitem in groceryitem_list])
	return render(request, "index.html", context)

def groceryitem(request, groceryitem_id):
	# response = f"You are viewing the details of {groceryitem_id}"
	# return HttpResponse(response)

	groceryitem = model_to_dict(GroceryItem.objects.get(pk = groceryitem_id))

	return render(request, "groceryitem.html", groceryitem)


def register(request):
	
	users = User.objects.all()
	is_user_registered = False

	user = User()
	user.username = "johndoe"
	user.first_name = "john"
	user.last_name = "doe"
	user.email = "john@mail.com"
	user.set_password("john1234")
	user.is_staff = False
	user.is_active = True

	for indiv_user in users:
		if indiv_user.username == user.username:
			is_user_registered = True
			break

	if is_user_registered == False:
		# to save our user
		user.save()

	context = {
		'is_user_registered': is_user_registered,
		'first_name': user.first_name,
		'last_name': user.last_name
	}

	return render(request, "register.html", context)

def change_password(request):

	is_user_authenticated = False

	user = authenticate(username = "johndoe", password = "john1234")

	if user is not None:
		authenticated_user = User.objects.get(username = 'johndoe')
		authenticated_user.set_password("johndoe1234567")
		authenticated_user.save()

		is_user_authenticated = True

	context = {
		"is_user_authenticated" : is_user_authenticated
	}

	return render(request, "change_password.html", context)

def login_user(request):
	username = 'johndoe'
	password = 'johndoe1234567'

	user = authenticate(username = username, password = password)

	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		return render(request, "login.html")

def logout_user(request):
	logout(request)
	return redirect("index")